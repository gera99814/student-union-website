export { default as ContactSection } from '../../components/ContactSection.vue'
export { default as Dropdown } from '../../components/Dropdown.vue'
export { default as Footer } from '../../components/Footer.vue'
export { default as MobilNavBar } from '../../components/MobilNavBar.vue'
export { default as NavBar } from '../../components/NavBar.vue'
export { default as NumInput } from '../../components/NumInput.vue'
export { default as ProfileCard } from '../../components/ProfileCard.vue'
export { default as Button } from '../../components/buttons/Button.vue'

export const LazyContactSection = import('../../components/ContactSection.vue' /* webpackChunkName: "components/ContactSection" */).then(c => c.default || c)
export const LazyDropdown = import('../../components/Dropdown.vue' /* webpackChunkName: "components/Dropdown" */).then(c => c.default || c)
export const LazyFooter = import('../../components/Footer.vue' /* webpackChunkName: "components/Footer" */).then(c => c.default || c)
export const LazyMobilNavBar = import('../../components/MobilNavBar.vue' /* webpackChunkName: "components/MobilNavBar" */).then(c => c.default || c)
export const LazyNavBar = import('../../components/NavBar.vue' /* webpackChunkName: "components/NavBar" */).then(c => c.default || c)
export const LazyNumInput = import('../../components/NumInput.vue' /* webpackChunkName: "components/NumInput" */).then(c => c.default || c)
export const LazyProfileCard = import('../../components/ProfileCard.vue' /* webpackChunkName: "components/ProfileCard" */).then(c => c.default || c)
export const LazyButton = import('../../components/buttons/Button.vue' /* webpackChunkName: "components/buttons/Button" */).then(c => c.default || c)
