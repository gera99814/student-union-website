import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from '@nuxt/ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _0eb50d92 = () => interopDefault(import('../pages/about/index.vue' /* webpackChunkName: "pages/about/index" */))
const _19261fa0 = () => interopDefault(import('../pages/calculator.vue' /* webpackChunkName: "pages/calculator" */))
const _0e293ae2 = () => interopDefault(import('../pages/contact.vue' /* webpackChunkName: "pages/contact" */))
const _0dcacdd8 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _3c07bdc7 = () => interopDefault(import('../pages/events.vue' /* webpackChunkName: "pages/events" */))
const _862084d6 = () => interopDefault(import('../pages/generator.vue' /* webpackChunkName: "pages/generator" */))
const _58109a9b = () => interopDefault(import('../pages/gt.vue' /* webpackChunkName: "pages/gt" */))
const _f81715e8 = () => interopDefault(import('../pages/services.vue' /* webpackChunkName: "pages/services" */))
const _0cfe6c4a = () => interopDefault(import('../pages/about/_slug.vue' /* webpackChunkName: "pages/about/_slug" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _0eb50d92,
    name: "about___hu"
  }, {
    path: "/calculator",
    component: _19261fa0,
    name: "calculator___hu"
  }, {
    path: "/contact",
    component: _0e293ae2,
    name: "contact___hu"
  }, {
    path: "/en",
    component: _0dcacdd8,
    name: "index___en"
  }, {
    path: "/events",
    component: _3c07bdc7,
    name: "events___hu"
  }, {
    path: "/generator",
    component: _862084d6,
    name: "generator___hu"
  }, {
    path: "/gt",
    component: _58109a9b,
    name: "gt___hu"
  }, {
    path: "/services",
    component: _f81715e8,
    name: "services___hu"
  }, {
    path: "/en/about",
    component: _0eb50d92,
    name: "about___en"
  }, {
    path: "/en/calculator",
    component: _19261fa0,
    name: "calculator___en"
  }, {
    path: "/en/contact",
    component: _0e293ae2,
    name: "contact___en"
  }, {
    path: "/en/events",
    component: _3c07bdc7,
    name: "events___en"
  }, {
    path: "/en/generator",
    component: _862084d6,
    name: "generator___en"
  }, {
    path: "/en/gt",
    component: _58109a9b,
    name: "gt___en"
  }, {
    path: "/en/services",
    component: _f81715e8,
    name: "services___en"
  }, {
    path: "/en/about/:slug",
    component: _0cfe6c4a,
    name: "about-slug___en"
  }, {
    path: "/about/:slug",
    component: _0cfe6c4a,
    name: "about-slug___hu"
  }, {
    path: "/",
    component: _0dcacdd8,
    name: "index___hu"
  }],

  fallback: false
}

function decodeObj(obj) {
  for (const key in obj) {
    if (typeof obj[key] === 'string') {
      obj[key] = decode(obj[key])
    }
  }
}

export function createRouter () {
  const router = new Router(routerOptions)

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    const r = resolve(to, current, append)
    if (r && r.resolved && r.resolved.query) {
      decodeObj(r.resolved.query)
    }
    return r
  }

  return router
}
