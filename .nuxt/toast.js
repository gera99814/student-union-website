import { createToastInterface } from "vue-toastification";

export default function (ctx, inject) {
  const toast = createToastInterface({"cssFile":false,"timeout":2000,"closeOnClick":false});
  inject('toast', toast);
}
