<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="assets/images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">University of Pécs - Faculty of Sciences Student Union Website</h3>
  <b align="center">
    My example project
  </b>
  <p align="center">
    This is a copy of the original repository
    <br />
    <br />
    <a href="https://ptettkhok.hu"><strong>View Live Website»</strong></a>
  </p>
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

Earlier I was event manager and social media manager at the Student Union of University of Pécs. We wanted a website so We summarized what pages, features we need and then I created a custom webdesign using the adobe xd design platform. After design consultations and meetings, I developed our custom website as a Vue Single Page Application.

Here's why:
* Students can ask us for help with their university studies and problems.
* They can find out information about our team, our services and our events.
* This website helps students calculate their special study average for scholarships and colleges with the average calculator.
* They get an insight into our Welcome Camp 

Of course, the project is not yet fully completed, for example, it is being translated into english and a few more new features will be coming.


### Built With

Here are some important technologies I used for the project:

* [Vue.js](https://vuejs.org/)
* [Nuxt.js](https://nuxtjs.org/)
* [Node.js](https://nodejs.org/en/)
* [SCSS](https://sass-lang.com/)
* [Hotjar](https://www.hotjar.com/)
* [Google Analytics](https://analytics.google.com/)



<!-- GETTING STARTED -->
## Getting Started

Here are some instructions to setting up the project locally.

### Installation

1. Clone the repository
   ```sh
   git clone https://gitlab.com/gera99814/student-union-website.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Run the Application
   ```sh
   npm run dev
   ```

4. Open http://localhost:3000/ in your browser


<!-- CONTACT -->
## Contact

Bence Geresdi - geresdibence@gmail.com

<p align="right">(<a href="#top">back to top</a>)</p>
