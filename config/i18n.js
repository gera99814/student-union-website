import en from '../locales/en.json'
import hu from '../locales/hu.json'
export default {
  locale: 'hu',
  fallbackLocale: 'hu',
  messages: { en, hu }
}
