---
img: "balazs.png"
position: "Földrajzi és Földtudományi Intézet"
name: "Magyar Balázs"
email: "balugrizmo@gmail.com"
mobil: "+36 30 394 2562"
facebook: "https://www.facebook.com/balazs.magyar.545"
instagram: "https://www.instagram.com/bagyar_mazsi/"
teams: "https://www.teams.com"
management: "no"
index: "3"

---

## Sziasztok! Magyar Balázs vagyok, harmad éves földrajz bsc szakos hallgató, valamint 2. éve tölthetem be az intézet hallgatói képviselői pozíciót. Kérdéseitekkel keressetek bizalommal e-mailen vagy Facebookon.
<!-- 

### PROFILKEP ###
A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! 
A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak

### LISTAZASI SORREND ###
Az "index" változó jelzi, hogy hanyadikként szerepel a felsorolásban
-->


