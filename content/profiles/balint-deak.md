---
img: "balint.png"
position: "Sporttudományi és Testnevelési Intézet"
name: "Deák Bálint"
email: "balintdeak1997@gmail.com"
mobil: "+36 20 360 0925"
facebook: "https://www.facebook.com/profile.php?id=100008318144282"
instagram: "https://www.instagram.com/balideak_26/"
teams: "https://www.teams.com"
management: "no"
index: "7"

---
## Sziasztok! Deák Bálint vagyok, a Sporttudományi és Testnevelési Intézet Hallgatói képviselője, másodéves sportszervező szakos hallgató. Célom az idei évben a tudományos diák – és sportkörök kialakítása, a hallgatói közélet fellendítése. Bárminemű problémáddal fordulj hozzám bizalommal!
<!-- 

### PROFILKEP ###
A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! 
A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak

### LISTAZASI SORREND ###
Az "index" változó jelzi, hogy hanyadikként szerepel a felsorolásban
-->


