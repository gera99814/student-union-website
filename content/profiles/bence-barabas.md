---
img: "bence.jpg"
position: "Osztatlan Tanárszak"
name: "Barabás Bence"
email: "bence100895@hotmail.com"
mobil: "+36 20 383 5806"
facebook: "https://www.facebook.com/barabas.bence.9"
teams: "https://www.teams.com"
management: "no"
index: "2"

---

## A HÖK és az osztatlanos hallgatók csapatát erősítem ebben az évben. Mivel már „veteránnak” számítok az egyetemi életben, ezért úgy gondoltam, hogy van elég tapasztalatom ahhoz, hogy másoknak is tudjak segíteni a tanulmányaik haladásában. Célom a pozíció betöltése alatt, hogy megkönnyítsem az egyetemi éveiteket bizonyos problémák megoldásával, tanár-diák kommunikációs elakadás megkönnyítése, minden aktuális és fontos információ megosztása veletek. Illetve rendezvények szervezése és azoknak lebonyolítása.

<!-- 

### PROFILKEP ###
A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! 
A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak

### LISTAZASI SORREND ###
Az "index" változó jelzi, hogy hanyadikként szerepel a felsorolásban
-->


