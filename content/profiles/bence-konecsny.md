---
img: "kony.jpg"
position: "Elnök"
name: "Konecsny Bence"
email: "konecsny.bence@pte.hu"
mobil: "+36 30 720 2229"
facebook: "https://facebook.com/b.konecsny"
instagram: "https://www.instagram.com/kony_0o0/?hl=hu"
teams: "https://www.teams.com"
management: "yes"
index: "1"
---

## "A jövőt azok határozzák meg, akik hajlandók azt formázni." A legtöbb dologhoz így állok hozzá. Célom az, hogy tapasztalataim és kreativitásom révén hozzájáruljak az előrehaladáshoz. Jelenthet ez egy eseményszervezést, korrepetálást vagy akár apróbb üzenetváltásokat is. Ha egy apró tettel is, de elősegítettem egy probléma megoldását vagy hozzájárultam a sikerhez, már megérte ráfordítani az időt. Veterán proginfós hallgatóként 2017 óta erősítem a Hallgatói Önkormányzat csapatát.  Eleinte matek-infós hallgatói képviselőként, majd informatikus megbízottként tevékenykedtem. Jelenleg elnökként dolgozom azon, hogy a hallgatókat összekössem egymással, a karral és a tanáraikkal. Ha vannak ötleteitek, amikről beszélnétek, keressetek! Ha tanulmányi gondotok adódott, írjatok! Ha visszajelzést adnátok munkásságunkról, csupa fül vagyok! Ha Kiskorsóban találkoznánk, koccintunk! 48-61-20-65-7A-74-20-6F-6C-76-61-73-6F-64-2C-20-6B-6F-6E-67-72-61-75-6C-E1-6C-6F-6B-21-20-49-74-74-20-61-20-6A-75-74-61-6C-6D-61-64-3A-20-68-74-74-70-73-3A-2F-2F-77-77-77-2E-79-6F-75-74-75-62-65-2E-63-6F-6D-2F-77-61-74-63-68-3F-76-3D-41-79-4F-71-47-52-6A-56-74-6C-73

<!-- A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak-->
