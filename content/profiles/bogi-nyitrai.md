---
img: "bogi.png"
position: "HR ügyekért felelős referens"
name: "Nyitrai Boglárka"
email: "boginyitrai@gmail.com"
mobil: "+36 30 900 0955"
facebook: "http://facebook.com/nyitrai.bogi"
teams: "teams.com"
management: "yes"
index: "3"
---

## Nyitrai Bogi vagyok, Kémia BSc szakos hallgató. Először 2019-ben mint a Kémiai Intézet Megbízott Hallgatói Képviselője kerültem be a Hallgatói Önkormányzat berkeibe, majd 2020 szeptemberétől Tanulmányi Referensként képvisellek titeket különböző bizottságokban és tanácsokban. Legfontosabb feladatomnak tartom, hogy megkönnyítsem számotokra az egyetemi élettel járó ügyintézést, segítsek nektek a problémás hallgatói ügyek gördülékeny megoldásában és szükség esetén végig vezesselek benneteket az egyetemi szabályzatok útvesztőjében. Bátran keressetek ügyelési időben, vagy a megadott elérhetőségeken!

<!-- A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak-->