---
img: "dalma.jpeg"
position: "Elnökhelyettes"
name: "Szigeti Dalma"
email: "szigeti.dalma@gmail.com"
mobil: "+36 30 579 0334"
facebook: "https://www.facebook.com/dalma.szigeti.5"
instagram: "https://www.instagram.com/szigeti.dalma/"
teams: "teams.com"
management: "yes"
index: "2"
---


## Szigeti Dalma vagyok, harmadéves matematika-fizika osztatlan tanárszakos hallgató. Nagy örömömre szolgál, hogy az 2021-es évtől én is a PTE TTK HÖK csapatát erősíthetem. Egész életemben egy nagyon aktív közösségi ember voltam, aki nem csak tagja szeretett lenni egy közösségnek, hanem szerettem is munkálkodni értük. Megtisztelő, hogy ezentúl a TTK család tagjaként, értünk, a ti érdekeitekért munkálkodhatok. Szeretném, ha minden hallgató boldogan töltené el nálunk egyetemi éveit, és a tanulás mellett, egyéb élményekkel is gazdagodhatna. Megígérhetem, hogy minden tőlem telhetőt megteszek, hogy ez megvalósuljon! Ha bármi problémátok, vagy nehézségetek adódna, ne habozzatok megkeresni, bármikor állok rendelkezésetekre, és együtt mindent megoldunk!

<!-- A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak-->
