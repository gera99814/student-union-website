---
img: "panka.jpeg"
position: "Tanulmányi referens"
name: "Riszt Edina"
email: "edina.panka@gmail.com"
mobil: "+36706488501"
facebook: "https://www.facebook.com/edinapanka.riszt"
teams: "teams.com"
management: "yes"
index: "4"
---

## Riszt Edina Panka vagyok, 2019 óta Természettudományi kar gazdaságinformatikus BSc szak hallgatója, 2021. szeptemberétől én töltöm be a Tanulmányi Referens pozíciót a HÖK-ön. Ezáltal az a megtiszteltetés ért, hogy titeket, hallgatókat képviselhetlek több fórumon is, ezen feladatomat pedig igyekszem a tőlem telhető legmagasabb színvonalon ellátni, hogy számotokra minél kisebb akadályt jelentsen az egyetemi ügyintézés. Törekszem arra, hogy segítsek nektek megoldást találni a különböző problémáitokra. Keressetek nyugodtan online vagy offline formában a HÖK-ön vagy a megadott elérhetőségeken!

<!-- A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak-->
