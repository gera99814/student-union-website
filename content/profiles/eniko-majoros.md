---
img: "eniko.png"
position: "Matematikai és Informatikai Intézet"
name: "Majoros Enikő"
email: "majoros.eniko9@gmail.com"
mobil: "+36 30 896 4448"
facebook: "https://www.facebook.com/eniko.majoros.5"
teams: "https://www.teams.com"
management: "no"
index: "6"

---
## Sziasztok! Majoros Enikő vagyok, másodéves Gazdaságinformatikai Bsc hallgató. Képviselőségem során szeretnék minél többet segíteni Nektek, hallgatóknak, legyen szó akár a tárgyfelvételről, vizsgázásról vagy a mostanában nem kevés nehézséget okozó csoportbontásról. Továbbá fontosnak tartom még a pezsgő egyetemi életet, így igyekszem majd különböző programokkal színesíteni a féléveiteket. Ha bármi kérdés felmerült bennetek, akkor keressetek bátran facebookon.
<!-- 

### PROFILKEP ###
A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! 
A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak

### LISTAZASI SORREND ###
Az "index" változó jelzi, hogy hanyadikként szerepel a felsorolásban
-->


