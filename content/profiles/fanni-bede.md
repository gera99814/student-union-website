---
img: "fanni-min.jpg"
position: "Kémiai Intézet"
name: "Bede Fanni"
email: "fanni.bede@gmail.com"
mobil: "+36 30 432 9564"
facebook: "https://www.facebook.com/fanni.bede"
teams: "https://www.teams.com"
management: "no"
index: "4"

---

## Végzős kémia szakos hallgatóként az egyetemi éveim alatt sok tapasztalatot szereztem a képzésünkről és a tanárokkal való kommunikáció sem újdonság számomra. Megbízható vagyok, szeretek csapatban dolgozni, de önállóan is megállom a helyemet, nyitott és barátságos embernek tartom magamat.
<!-- 

### PROFILKEP ###
A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! 
A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak

### LISTAZASI SORREND ###
Az "index" változó jelzi, hogy hanyadikként szerepel a felsorolásban
-->


