---
img: "isti.jpeg"
position: "Kulturális referens"
name: "Telek Isti"
email: "kistelek0622@gmail.com"
mobil: "+36302269742"
facebook: "https://www.facebook.com/telek.isti1"
instagram: "https://www.instagram.com/telekisti/"
teams: "teams.com"
management: "yes"
index: "6"

---

## Telek István vagyok a PTE TTK HÖK csapatának kulturális referense. Feladatim közé tartozik a TTK hallgatók életének színesebbé tétele. A fő feladataim a TTK rendezvények megszervezése, a közösségi médiákon keresztül történő információk átadása és a partnereinkkel való kapcsolattartás, új partnerekkel való kapcsolatok kiépítése. A hét legtöbb napján megtalálható vagyok a HÖK irodában, ahol a hallgatók kérdéseivel, újító ötleteikkel szívesen foglalkozok. Keressetek bizalommal.

<!-- 

### PROFILKEP ###
A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! 
A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak


### LISTAZASI SORREND ###
Az "index" változó jelzi, hogy hanyadikként szerepel a felsorolásban
-->


