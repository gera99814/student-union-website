---
img: "luis.jpg"
position: "Külföldi Hallgatók"
name: "Nasi Luis"
email: "luisnasi96@gmail.com"
mobil: "+36 30 251 8784"
facebook: "https://www.facebook.com/luis.nasi.1"
teams: "https://www.teams.com"
management: "no"
index: "5"

---
## Nasi Luis vagyok, az intézetünk külföldi hallgatói a képviselője. Erre a pozícióra 2018-ban lettem megválasztva, ugyanis karunkon majdnem kettőszáz külföldi hallgató folytatja tanulmányait. Az egyik célom, hogy rendezvényeket szervezzek nekik, angol nyelven. Úgy gondolom, ez nem csak a külföldieknek lehet érdekes és hasznos, hanem a magyaroknak is. Bármilyen kérdés vagy ötlet esetén keressetek bizalommal!
<!-- 

### PROFILKEP ###
A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! 
A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak

### LISTAZASI SORREND ###
Az "index" változó jelzi, hogy hanyadikként szerepel a felsorolásban
-->


