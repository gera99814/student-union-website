---
img: "nori.png"
position: "Biológiai Intézet"
name: "Kalocsai Nóri"
email: "kalocsainora@gmail.com"
mobil: "+36 70 594 3698"
facebook: "https://www.facebook.com/nora.kalocsai.1"
teams: "teams.com"
management: "no"
index: "1"

---

## Kalocsai Nóra vagyok, másodéves biológus BSc hallgató és a Biológiai Intézet hallgatói képviselője. Az egyetemi évek sok mindenről szólnak, tanulásról, szórakozásról és új barátokról, így hát hallgatói képviselőként azon fogok tevékenykedni, hogy az esetleges problémáitokra gyors megoldást, kérdéseitekre pedig választ találjunk. Továbbá, hogy közösségépítő és minél színesebb programkínálattal szolgálhassunk nektek. Ha bármilyen kérdésetek van, bátran forduljatok hozzám!

<!-- 

### PROFILKEP ###
A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! 
A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak

### LISTAZASI SORREND ###
Az "index" változó jelzi, hogy hanyadikként szerepel a felsorolásban
-->


