---
img: "adi.png"
position: "Kommunikációs referens"
name: "Pongrácz Ádám"
email: "pongradam@gmail.com"
mobil: "+36 70 356 9282"
facebook: "http://facebook.com/pongradam"
instagram: "http://instagram.com/adi_theaven"
youtube: "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
teams: "teams.com"
management: "yes"
index: "5"
---

## Pongrácz Ádám vagyok, másodéves programtervező informatikus BSc hallgató. A PTE TTK HÖK tagjaként én látom el a kommunikációs feladatokat, mely többek között lefedi a rendszergazdai teendőket, események grafikai identitásának megtervezését, illetve ezen események marketingjét. Hogyha egyetemi eseményt szeretnél szervezni, készségesen segítek, akár borítóképről, plakátról, videóról, vagy egyéb grafikai munkáról lenne szó. Keress bátran ügyelési időmben, vagy az alábbi elérhetőségeken!

<!-- A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak-->


