---
img: "raja.png"
position: "Külügyi Referens"
name: "Kelemen Rajmund"
email: "kelemenrajmund@gmail.com"
mobil: "+36 20 577 2288"
facebook: "http://facebook.com/rajmund.kelemen"
instagram: "http://instagram.com/kraja2"
teams: "teams.com"
management: "yes"
index: "7"
---

## Kelemen Rajmund vagyok, végzős biológia-földrajz tanár szakos hallgató. A PTE TTK HÖK külügyi referensi pozícióját töltöm be, így feladataim közé tartozik a karunkra érkező külföldi hallgatók orientálása valamint a velük való kapcsolattartás. Első kézből tudom, hogy milyen nehéz egy új országban idegen nyelvű környezetbe kerülni, így legjobb képességeim szerint igyekszem az ide érkező hallgatókat segíteni, hogy el tudjanak igazodni az egyetem rendszerében valamint a városban. Ha kérdésed van, keress nyugodtan, ügyelési időben, fant a fent említett elérhetőségek egyikén.

<!-- A "profiles" mappában hozz létre egy új .md filet és kövesd az itt látható szintaxist! A profilképet helyezd el az assets/images mappában és itt add meg az "img" változónak-->
