require('dotenv').config()
import i18n from './config/i18n'

export default {
  googleAnalytics: {
    id: 'UA-193502140-1'
  },
  server: {
    port: process.env.PORT || 3000 // default: 3000
  },
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'PTE TTK HÖK',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: "~/plugins/vue2-google-maps.js" },
    { src: '~/plugins/aos', ssr: false },
    { src: '~/plugins/toast', ssr: false },
    { src: '~/plugins/vue-fb-customer-chat.js', ssr: false },
    { src: '~/plugins/vue-aglie', ssr: false},
    { src: '~/plugins/hotjar.js', mode: 'client'}
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    [
      'nuxt-i18n',
      {
        vueI18nLoader: true,
        defaultLocale: 'hu',
         locales: [
          {
            code: 'en',
            name: 'English'
          },
          {
            code: 'hu',
            name: 'Hungarian'
          }
        ],
        vueI18n: i18n
      }
     ]
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    [
      'nuxt-fontawesome', {
      imports: [
       {
         set: '@fortawesome/free-solid-svg-icons',
         icons: ['fas']
       },
       {
         set:'@fortawesome/free-brands-svg-icons',
         icons: ['fab']
       }
     ]
    }
    ],
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    '@nuxt/content',
    "vue-toastification/nuxt",
    '@nuxtjs/dotenv',
    '@nuxtjs/google-analytics',
    '@nuxtjs/i18n'

  ],

  toast: {
    // Or disable CSS injection
    timeout: 2000,
    closeOnClick: false,
    cssFile: false
  },


  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    vendor: ["vue2-google-maps","aos"],
    transpile: [/^vue2-google-maps($|\/)/, 'vue-aglie'],
  },

  css: [
    "@/assets/main.scss",
    "aos/dist/aos.css"

  ],

  styleResources:{
    scss: [
      '@/assets/tokens/colors.scss',
      '@/assets/tokens/font-sizes.scss',
      '@/assets/tokens/spacing.scss',
      '@/assets/mixins/mixins.scss',
      '@/assets/tokens/radius.scss',
    ]
  },

  generate: {
    dir: 'dist'
  },
  // target: 'static',
  // router: {
  //   base: 'hok'
  // }
}
