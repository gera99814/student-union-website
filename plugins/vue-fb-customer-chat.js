import Vue from 'vue'
import VueFbCustomerChat from 'vue-fb-customer-chat'
 
Vue.use(VueFbCustomerChat, {
  page_id: 150123858380403, //  change 'null' to your Facebook Page ID,
  theme_color: '#000', // theme color in HEX
  locale: 'hu_HU', // default 'en_US'
})
